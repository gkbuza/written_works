## This GitLab link
https://gitlab.com/gkbuza/written_works

## Name
Collection of written works during MSc.

## Description
This is a collection of some written works I wrote during my master's at the University of Ljubljana. It consists of PDFs containing some elaborate problems and solutions from different courses from the master's track, and two university publications in the so-called Matrika journal (http://matrika.fmf.uni-lj.si).

## Contributing
Corrections are much welcomed. Letting me know about typos would be much appreciated. Telling me what you consider worth saying would be much appreciated too.

## Contact
gkbnotes@gmail.com